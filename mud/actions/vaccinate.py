from .action import Action2
from mud.events import VaccinateEvent

class VaccinateAction(Action2):
    EVENT = VaccinateEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "vaccinate"