from .event import Event2

class DrinkEvent(Event2):
    NAME = "drink"
    def perform(self):
        self.object.move_to(None)
        self.inform("drink")
