from .event import Event2

class VaccinateEvent(Event2):
    NAME = "vaccinate"
    def perform(self):
        self.object.move_to(None)
        self.inform("vaccinate")